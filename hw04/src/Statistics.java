import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Statistics {
    public static void main(String[] args) {
        int numStep = 3;
        int countStep = 0;
        int distanceKm;
        Scanner scanner = new Scanner(System.in);

        List<String> date = new ArrayList<>();
        List<String> time = new ArrayList<>();
        List<Integer> distance = new ArrayList<>();

        int distanceOfMeter;
        double distanceInMiles;
        String hours;
        String minutes;
        Float tempMinutes;
        double tempHource;

        while (numStep != countStep) {
            countStep++;

            System.out.println("Укажите дату в формате ДД.ММ.ГГ");
            date.add(scanner.nextLine());
            System.out.println("Укажите время потраченое на поездку в формате ЧЧ:ММ");
            time.add(scanner.nextLine());
            System.out.println("Сколько км Вам удалось проехать:");
            distanceKm = Integer.parseInt(scanner.nextLine());
            distance.add(distanceKm);
        }

        for (int i = 0; i < numStep; i++) {
            distanceOfMeter = distance.get(i) * 1000;
            distanceInMiles = distance.get(i) / 1.6;
            hours = time.get(i).substring(0, 2);
            minutes = time.get(i).substring(3, 5);
            tempMinutes = Float.parseFloat(hours) * 60 + Float.parseFloat(minutes);
            tempHource = tempMinutes/60;

            String msg = "Дата: " + date.get(i) + ". Пройдено пути " + distance.get(i) +
                    "км (" + distanceOfMeter + " метров/" + distanceInMiles +
                    " Миль). Пройдено за: " + tempMinutes + " минут. Скорость " +
                    Math.ceil(distance.get(i)/tempHource) + "km/ч (" +
                    Math.ceil(distanceInMiles/tempHource) + "mph)";
            // Максим, прошу простить мне отсутствие потрачкных калорий, я устал от посчетов в строках  с 36 по 47

            System.out.println(msg);
        }

    }
}
