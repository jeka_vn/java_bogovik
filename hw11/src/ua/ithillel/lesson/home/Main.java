package ua.ithillel.lesson.home;

import static ua.ithillel.lesson.home.Home.showNumResidents;

public class Main {
    public static void main(String[] args) {
        int[][] arrResidents = new int[5][3];

        Home home = new Home();

        home.setTenants(new int[]{12, 67, 78});
        arrResidents[0] = home.getTenants();
        home.setTenants(new int[]{34, 1, 9});
        arrResidents[1] = home.getTenants();
        home.setTenants(new int[]{34, 1, 56});
        arrResidents[2] = home.getTenants();
        home.setTenants(new int[]{65, 87, 0});
        arrResidents[3] = home.getTenants();
        home.setTenants(new int[]{1, 99, 56});
        arrResidents[4] = home.getTenants();

        int result = 0;
        for (int i = 0; i < arrResidents.length; i++) {
            int calc = 0;
            for (int i1 = 0; i1 < arrResidents[i].length; i1++) {
                calc += arrResidents[i][i1];
                if (result < calc) result = calc;
            }
        }

        showNumResidents(result);

    }
}
