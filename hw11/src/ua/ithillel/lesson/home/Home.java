package ua.ithillel.lesson.home;

import java.util.Arrays;

public class Home {
    private int lengthArr = 0;
    private int[] tenants;
    private static int numResidents = 0;

    public static void showNumResidents(int numResidents) {
        System.out.println(numResidents);
    }

    public int[] getTenants() {
        return tenants;
    }

    public void setTenants(int[] arr) {
        lengthArr = arr.length;
        tenants = new int[lengthArr];
        System.arraycopy(arr, 0, tenants, 0, arr.length);
    }


}
