package ua.hillel.lessonn.hw14;

class Transport {
    private int numPersons = 0;
    private int distance = 0;
    private final double TRANSPORTATION_COST_KM = 0.55;


    public void freightPriceCalc() {
        double result = Math.round(this.distance * this.TRANSPORTATION_COST_KM);
        System.out.println("Цена перевозки: " + result + " $ за " + numPersons + " кг груза.");
    }

    Transport(final int numPersons, final int distance) {
        this.numPersons = numPersons;
        this.distance = distance;
    }
}
