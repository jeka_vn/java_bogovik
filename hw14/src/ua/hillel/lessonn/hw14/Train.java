package ua.hillel.lessonn.hw14;

public class Train extends Transport {
    Train(int numPersons, int distance) {
        super(numPersons, distance);
    }

    private String fuelType = "electric";
    private boolean engine = false;
    private boolean trafficPermit = false;

    public void engining() {
        if (!this.trafficPermit) {
            System.out.println("Перед тем как готовить поез к поездке, нужно получить разрешение на движение");
        } else {
            if (this.engine) {
                this.engine = false;
                System.out.println("Поезд не может продолжить движение т.к вы выключили двигатель");
            } else {
                this.engine = true;
                System.out.println("Поезд готов к движению");
            }
        }
    }

    public void fuelSwitching() {
        if (this.fuelType.equals("electric")) {
            this.fuelType = "diesel";
            System.out.println("Вы переключили поезд на " + this.fuelType);
        } else {
            this.fuelType = "electric";
            System.out.println("Вы переключили поезд на " + this.fuelType);
        }
    }

    public void getTrafficPermit() {
        this.trafficPermit = true;
        System.out.println("Разрешение на запуск и движение получено!");
    }
}
