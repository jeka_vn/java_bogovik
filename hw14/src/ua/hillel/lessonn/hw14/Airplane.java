package ua.hillel.lessonn.hw14;

public class Airplane extends Transport {
    Airplane(int numPersons, int distance) {
        super(numPersons, distance);
    }
    private boolean CHECK_TECHNICAL_CONDITION = false;
    private int fuelSizeTank = 0;
    private boolean permissionFly = false;


    public void check_condition() {
        this.CHECK_TECHNICAL_CONDITION = true;
        System.out.println("Самолет проверен");
    }

    public void gasStation() {
        this.fuelSizeTank = 5000;
        System.out.println("Самолет заправлен");
    }

    public void getPermissionFly() {
        if ((this.fuelSizeTank > 0) && (this.CHECK_TECHNICAL_CONDITION != false)) {
            System.out.println("Вылет разрешен");
        } else if (this.fuelSizeTank == 0) {
            System.out.println("Перед вылетом нужно заправить самолет, камикадце.");
        } else if (this.CHECK_TECHNICAL_CONDITION != true) {
            System.out.println("Перед вылетом нужно проверить состояние самолета");
        }
    }
}
