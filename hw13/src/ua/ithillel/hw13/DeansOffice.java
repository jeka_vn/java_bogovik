package ua.ithillel.hw13;

import java.util.Arrays;

public class DeansOffice {
    Students students = new Students();

    public void searchByName(String[][] students, String partName) {
        for (int i = 0; i < students.length; i++) {
            for (String item: students[i]) {
                if (item.toLowerCase().equals(partName.toLowerCase())) {
                    System.out.println(Arrays.toString(students[i]));
                    break;
                }
            }
        }
    }

    public void searchByFullName(String[][] students, String name, String midName, String surname) {
        for (int i = 0; i < students.length; i++) {
            for (String item: students[i]) {
                if ((item.toLowerCase().equals(surname.toLowerCase()))) {
                    for (String item1: students[i]) {
                        if ((item1.toLowerCase().equals(midName.toLowerCase()))) {
                            for (String item2: students[i]) {
                                if ((item2.toLowerCase().equals(name.toLowerCase()))) {
                                    System.out.println(Arrays.toString(students[i]));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void searchByNum (String[][] students, int numStudent) {
        for (int i = 0; i < students.length; i++) {
            if (Integer.parseInt(students[i][5]) == numStudent) {
                System.out.println(Arrays.toString(students[i]));
                break;
            }
        }
    }

    public void transferCourse(String[][] students, int numStudent, Courses courses) {
        for (int i = 0; i < students.length; i++) {
            if (Integer.parseInt(students[i][5]) == numStudent) {
                students[i][0] = String.valueOf(courses);
                break;
            }
        }
    }

    public void changeStatus(String[][] students, int numStudent, StatusStudent status) {
        for (int i = 0; i < students.length; i++) {
            if (Integer.parseInt(students[i][5]) == numStudent) {
                students[i][1] = String.valueOf(status);
                break;
            }
        }
    }
}
