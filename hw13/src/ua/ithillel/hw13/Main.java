package ua.ithillel.hw13;

public class Main {
    public static void main(String[] args) {
        Student student = new Student();
        Students students = new Students();
        DeansOffice deansOffice = new DeansOffice();
        students.addStudent(
                student.createStudent(
                        Courses.JAVA,
                        StatusStudent.CREDITED,
                        "Eugen",
                        "Vasilevich",
                        "Lambov"
                )
        );
        students.addStudent(
                student.createStudent(
                        Courses.QA_MANUAL,
                        StatusStudent.CREDITED,
                        "Ed",
                        "none",
                        "Izotov"
                )
        );



        deansOffice.searchByName(students.getAllStudents(), "izotov");
//        deansOffice.searchByFullName(students.getAllStudents(), "Eugen", "Vasilevich", "Lambov");
//        deansOffice.searchByNum(students.getAllStudents(), 2);
//        deansOffice.transferCourse(students.getAllStudents(), 1, Courses.JAVASCRIPT);
//        System.out.println(Arrays.deepToString(students.getAllStudents()));
//        System.out.println(Arrays.deepToString(students.getAllStudents()));
//        deansOffice.changeStatus(students.getAllStudents(), 145, StatusStudent.EXPELLED);
//        System.out.println(Arrays.deepToString(students.getAllStudents()));
    }
}
