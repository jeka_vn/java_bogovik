package ua.hillel.lesson.hw15;
import java.util.Scanner;

public class Index {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите текст: ");
        String text = in.nextLine();
        Printer printer = new Printer();
        printer.print(text);
        printer = new RedColor();
        printer.print(text);
        printer = new BlueColor();
        printer.print(text);
        printer = new PurpleColor();
        printer.print(text);
        printer = new CyanColor();
        printer.print((text));
    }
}
