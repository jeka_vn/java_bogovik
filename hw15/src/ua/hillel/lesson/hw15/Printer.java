package ua.hillel.lesson.hw15;

public class Printer {

    protected String getTextColor() {
        return Color.ANSI_DEFAULT.getColor();
    }

    public void print(String bumaga) {
        System.out.println(getTextColor() + bumaga);
    }
}
