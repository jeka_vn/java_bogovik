package ua.hillel.lesson.hw15;

import static ua.hillel.lesson.hw15.Color.ANSI_BLUE;

public class BlueColor extends Printer {
    @Override
    protected String getTextColor() {
        return ANSI_BLUE.getColor();
    }
    public void print(String bumaga) {
        System.out.println(getTextColor() + bumaga);
    }
}
