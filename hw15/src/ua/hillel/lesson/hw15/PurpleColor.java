package ua.hillel.lesson.hw15;

import static ua.hillel.lesson.hw15.Color.ANSI_PURPLE;

public class PurpleColor extends Printer {
    @Override
    protected String getTextColor() {
        return ANSI_PURPLE.getColor();
    }
    public void print(String bumaga) {
        System.out.println(getTextColor() + bumaga);
    }
}
