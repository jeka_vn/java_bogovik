package ua.hillel.lesson.hw15;

import static ua.hillel.lesson.hw15.Color.ANSI_CYAN;

public class CyanColor extends Printer {
    @Override
    protected String getTextColor() {
        return ANSI_CYAN.getColor();
    }
    public void print(String bumaga) {
        System.out.println(getTextColor() + bumaga);
    }
}
