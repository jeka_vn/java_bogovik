package ua.hillel.lesson.hw15;

import static ua.hillel.lesson.hw15.Color.ANSI_RED;

public class RedColor extends Printer {
    @Override
    protected String getTextColor() {
        return ANSI_RED.getColor();
    }
    public void print(String bumaga) {
        System.out.println(getTextColor() + bumaga);
    }
}
