package ua.hillel.lesson.hw15;

public enum Color {
    ANSI_DEFAULT("\u001b[0m"),
    ANSI_RED("\u001b[31m"),
    ANSI_BLUE("\u001b[34m"),
    ANSI_PURPLE("\u001b[35m"),
    ANSI_CYAN("\033[4;33m");

    private final String color;

    Color(String color) {
        this.color = color;
    }

    public String getColor(){
        return color;
    }
}
