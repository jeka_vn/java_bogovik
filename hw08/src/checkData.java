import java.util.Scanner;

public class checkData {
    public static void main(String[] args) {
        String comand;
        Scanner sc = new Scanner(System.in);
        showMsg("Введите команду");
        comand = sc.nextLine();
        char[] commandArr = parseStr(comand);
        char[] commandArrSample = {'.', '-', '.', '-', ' ', '-', '-', '-', '.', ' ', '.', '.', '-'};

        checkData(commandArr, commandArrSample);
    }

    public static void checkData(char[] commandArr, char[] commandArrSample) {
        boolean check = true;
        for (int i = 0; i < commandArr.length; i++) {
            if (commandArr[i] != commandArrSample[i]) {
                check = false;
                break;
            }
        }

        if (check) {
            showMsg("Молодец!");
        } else {
            showMsg("Не, это не то");
        }

    }
    public static char[] parseStr(String comand) {
        return comand.toCharArray();
    }
    public static void showMsg(String msg) {
        System.out.println(msg);
    }
}
