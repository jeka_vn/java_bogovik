package ua.hillel.lesson.hw16;

import static ua.hillel.lesson.hw16.BuilderInterface.*;
import static ua.hillel.lesson.hw16.DocInterface.*;
import static ua.hillel.lesson.hw16.TeacherInterface.UnemployedTeacher;
import static ua.hillel.lesson.hw16.TeacherInterface.nameTeacher;

public class LaborExchange implements LabInterface {
    public static void allUnEmployedPeoples(Worker worker){
        if(!worker.isUnemployed()){
            System.out.println(worker.viewInfoMethod());
        }
    }
    public static void allEmployedPeoples(Worker worker){
        if(worker.isUnemployed()){
            System.out.println(worker.viewInfoMethod());
        }
    }
    public static void millionerDetector(Worker worker){
        if(worker.getSalary() >= 1000000){
            System.out.println(worker.viewInfoMethod());
        }
    }

    public static void main(String[] args) {
        Worker [] worker = new Worker[4];
        worker[0] = new Builder(nameBuilder, salaryBuilder, UnemployedBuilder);
        worker[1] = new Doc(nameDoctor, salaryDoctor, UnemployedDoctor);
        worker[2] = new Prog(nameDoctor, salaryDoctor, UnemployedDoctor);
        worker[3] = new Teacher(nameTeacher, salaryBuilder, UnemployedTeacher);

        // метод на получения всех людей, которые уже работают,
        System.out.println("Список тунеядцев : ");
        for (Worker value : worker) {
            allUnEmployedPeoples(value);
        }
        // метод на получения всех людей, которые еще не нашли работу,
        System.out.println("\t");
        System.out.println("Список ударников труда : ");
        for (Worker value : worker) {
            allEmployedPeoples(value);
        }
        // метод на получения людей в разрезе какой-то конкретной профессии
        System.out.println("\t");
        System.out.println("Список долларовых миллионеров : ");
        for (Worker value : worker) {
            millionerDetector(value);
        }
    }
}
