package ua.hillel.lesson.hw16;

public abstract class Worker implements LabInterface, BuilderInterface, DocInterface, ProgInterface, TeacherInterface{
    private String name;
    private int salary;
    private boolean unemployed;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public boolean isUnemployed() {
        return unemployed;
    }

    public void setUnemployed(boolean unemployed) {
        this.unemployed = unemployed;
    }

    Worker() {

    }

    Worker(String name, int salary, boolean unemployed) {
        this.name = name;
        this.salary = salary;
        this.unemployed = unemployed;
    }

    public String viewInfoMethod() {
        String status = "Безработный";
        if (isUnemployed()) status = "Работает";
        return getName() + "   " + getSalary() + "         " + status;
    }
}
