package ua.hillel.lesson.hw16;

public class Builder extends Worker implements LabInterface, BuilderInterface {
    Builder(){}

    Builder(String name, int salary, boolean unemployed) {
        super(name, salary, unemployed);
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public int getSalary() {
        return super.getSalary();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    public static void main(String[] args) {
        Builder[] builders = new Builder[1];
        builders[0] = new Builder(nameBuilder, salaryBuilder, UnemployedBuilder);
        for (int i = 0; i < builders.length; i++) {
            Builder builder = builders[i];
            System.out.println(builder.viewInfoMethod());
        }
    }
}
