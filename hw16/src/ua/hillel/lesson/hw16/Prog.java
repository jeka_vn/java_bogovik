package ua.hillel.lesson.hw16;

public class Prog extends Worker implements ProgInterface, LabInterface {
    Prog(){

    }

    Prog(String name, int salary, boolean unemployed) {
        super(name, salary, unemployed);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public int getSalary() {
        return super.getSalary();
    }

    public static void main(String[] args) {
        Prog[] programmers = new Prog[1];
        programmers[0] = new Prog(nameProg,salaryProg,UnemployedProg);
        for (Prog programmer : programmers) {
            System.out.println(programmer.viewInfoMethod());
        }
    }
}
