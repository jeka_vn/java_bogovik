import java.util.Scanner;

public class NumberLength {
    public static void main(String[] args) {
        getUserData();
    }

    // Метод получает данные пользователя
    private static void getUserData() {
        int userNum;
        Scanner sc = new Scanner(System.in);
        userNum = sc.nextInt();
        checkNum(userNum);
    }
    // Метод проверяет не больше ли число пользователя чем maxNum и посчитывает
    // количество цифр
    private static void checkNum (int userNum) {
        int maxNum = 2000;
        int userNumeric = Math.abs(userNum);

        if (userNumeric < maxNum) {
            int numLength = (int)(Math.log10(userNumeric)+1);
            showMsg("Длина числа: " + numLength);
        } else {
            showMsg("Вы ввели число больше: " + maxNum);
        }

    }
    // Метод выводит сообщение
    private static void showMsg(String msg) {
        System.out.println(msg);
    }


}
