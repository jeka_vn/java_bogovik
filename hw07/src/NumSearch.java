import java.util.Scanner;

public class NumSearch {

    public static void main(String[] args) {
        getUserData();
    }

    public static void getUserData() {
        int numOne = 0;
        int numTwo = 0;
        Scanner sc = new Scanner(System.in);

            showMsg("Введите число от 1000 до 9999");
            numOne = sc.nextInt();
            checkNum("one", numOne, numTwo);
            showMsg("Введите числе от 1 до 9");
            numTwo = sc.nextInt();
            checkNum("two", numOne, numTwo);
    }

    public static void checkNum (String instruction, int oneNum, int twoNum) {

        if (instruction.equals("one")) {
            if ((oneNum <= 9999) && (oneNum >= 1000)) {
                searchNum(oneNum, twoNum);
            } else {
                getUserData();
            }
        }
        if (instruction.equals("two")) {
            if ((twoNum <= 9) && (twoNum >= 1)) {
                searchNum(oneNum, twoNum);
            } else {
                getUserData();
            }
        }

    }

    public static void searchNum (int oneNum, int twoNum) {

        if (oneNum != 0 && twoNum != 0) {
            if ((oneNum / 1000 == twoNum) || (oneNum / 100 % 10 == twoNum) || (oneNum / 10 % 10 == twoNum) || (oneNum % 10 == twoNum)
            ) {
                showMsg("Число: " + twoNum + " найдено в цифре: " + oneNum);
            } else {
                showMsg("Число: " + twoNum + "  не найдено в цифре: " + oneNum);
            }
        }

    }
    public static void showMsg(String msg) {
        System.out.println(msg);
    }
}

