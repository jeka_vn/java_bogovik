package ua.ithillel.hw12;

public class Liquidity {
    private int temp = 0;
    private int boilingPoint = 100;
    private int freezingPoint = 0;
    private String color = "none";
    private String smell = "none";
    private String taste = "none";

    public static Liquidity SET_TEMP (int temp) {
        return new Liquidity(temp);
    }
    public static Liquidity SET_TEMP_COLOR_SMELL_TASTE (int temp, String color, String smell, String taste) {
        return new Liquidity(temp, color, smell, taste);
    }
    public static Liquidity SET_TEMP_BOILING_FREEZING (int temp, int boilingPoint, int freezingPoint) {
        return new Liquidity(temp, boilingPoint, freezingPoint);
    }

    private Liquidity (int temperature) {
        this.temp = temperature;
        water(temperature);
    }
    private Liquidity (int temp, String color, String smell, String taste) {
        this.temp = temp;
        this.color = color;
        this.smell = smell;
        this.taste = taste;
        water(temp);
    }
    private Liquidity (int temp, int boilingPoint, int freezingPoint) {
        this.temp = temp;
        this.boilingPoint = boilingPoint;
        this.freezingPoint = freezingPoint;
        water(temp);
    }

    private void water(int temp) {
        if (temp >= boilingPoint) {
            showMsg("Вода испарилась");
        } else if (temp <= freezingPoint) {
            showMsg("Вода замерзла");
        } else {
            showMsg("Температура воды: " + temp + "°");
        }
    }

    private static void showMsg(String msg) {
        System.out.println(msg);
    }
}
