import java.util.Scanner;

public class GetAndShowData {
    public static void main(String[] args) {
        getUserData();
    }

    private static void getUserData() {
        String numGroup;
        String[] userFullName = new String[3];

        Scanner sc = new Scanner(System.in);
        showMsg("Введите Ваше имя: ");
        userFullName[1] = sc.nextLine();
        showMsg("Введите Ваше отчество: ");
        userFullName[2] = sc.nextLine();
        showMsg("Введите Вашу фамилию: ");
        userFullName[0] = sc.nextLine();
        showMsg("Введите номер группы: ");
        numGroup = sc.nextLine();

        String fullName = " " + userFullName[0] + " " + userFullName[1] + " " + userFullName[2];
        String[] data = {
                " Практическое занятие № 1",
                " Выполнил(а): ст. гр. ЗИ-" + numGroup,
                fullName};
        showData(data);
    }

    private static void showData (String[] data) {
        int maxLengStr = getLeng(data) + 4;
        char symbol = '*';

        generateLine(maxLengStr, symbol);

        System.out.println();

        for (String datum : data) {
            int numSpase = maxLengStr - datum.length() - 5;
            String result = generate(numSpase);
            System.out.println(symbol + " " + datum + result + symbol);
        }

        generateLine(maxLengStr, symbol);

        System.out.println();
    }

    private static void generateLine(int maxLengStr, char symbol) {
        for (int j = 0; j < maxLengStr; j++) {
            System.out.print(symbol);
        }
    }

    private static String generate(int num) {
        StringBuilder spase = new StringBuilder(" ");
        for (int i = 0; i <= num; i++) {
            spase.append(" ");
        }
        return spase.toString();
    }

    private static int getLeng(String[] data) {
        int result = 0;
        for (String item: data) {
            if (item.length() > result) {
                result = item.length();
            }
        }
        return result;
    }

    private static void showMsg(String msg) {
        System.out.print(msg);
    }
}
